# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the bin/rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: "Star Wars" }, { name: "Lord of the Rings" }])
#   Character.create(name: "Luke", movie: movies.first)
require 'net/http'

def image_file(size: '300x300', format: 'png', background_color: :random, text_color: nil, text: nil)
  file = Tempfile.new("faker_placeholdit")
  file.binmode
  file << Net::HTTP.get(URI(Faker::Placeholdit.image(size: size, format: format, background_color: background_color, text_color: text_color, text: text)))
  file.close

  ::File.new(file.path)
end

Product.delete_all

5.times do
  Product.create(
    name: "bob",
  ).tap do |product|
    product.hero_image.attach(io: image_file, filename: "hero_image.png")
  end
end
